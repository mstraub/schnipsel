[TOC]

## Configuration

###  Migrate launch configurations

To migrate launch configs between different workspaces simply copy the .launch files from:

	/eclipse-workspace/.metadata/.plugins/org.eclipse.debug.core/.launches


### Organise Imports

- Automatically organise imports: Ctrl+Shift+O
- Static import: Ctrl+Shift+M

To exclude annoying packages or classes when doing automatic organisation of imports with Ctrl+Shift+O create a a "Type Filter" under `Windows -> Preferences -> Java -> Appearance -> Type Filters`.

My current list:

	scala.*
	java.awt.*
	junit.*
	org.joda.*


## SVN support

As of 2018 the subversive project seems to be dead, so we must use subclipse. Install it from the Eclipse Marketplace and then:

```bash
sudo apt install libsvnclientadapter-java
```

Specify the folder where `libsvnjavahl-1.so` is located as library path in `eclipse.ini` (the line must come directly after `-vmargs`)


	-vmargs
	-Djava.library.path=/usr/lib/x86_64-linux-gnu/jni


see also https://github.com/subclipse/subclipse/wiki/JavaHL-on-Linux

## Troubleshooting

see [KDE 5 / Kubuntu Tipps &amp; Tricks](kubuntu)

### Scroll bars hide content

Start with:

```bash
GTK_OVERLAY_SCROLLING=0
```

This will permanently reserve place for the scroll bars and no content will be hidden.

See https://bugs.eclipse.org/bugs/show_bug.cgi?id=519728

### Scroll bars too small

Using Kubuntu 20.04 I don't like the miniscule scroll bars that slightly widen when hovering over them. Setting a min size works as follows:

Edit .config/gtk-3.0/gtk.css and add these lines:


	.scrollbar.vertical slider, scrollbar.vertical slider {
	    min-width: 7px;
	}
	.scrollbar.horizontal slider, scrollbar.horizontal slider {
	    min-height: 7px;
	}


###  Autocompletion not working properly

This happens sometimes when upgrading a workspace to a new version of Eclipse (most recently to me when upgrading vom 2018.09 to 2018.12).

Window->Preferences->Java->Editor->Content Assist->Advanced

Now make sure the following are all ticked in the top and bottom sections:


- Java Non-Type Proposals
- Java Proposals
- Java Type Proposals

Apply the changes and then close and re-open your project and it will build the autocompletions which will then become available when you hit Ctrl-Space.

Source: https://www.chrisnewland.com/solved-eclipse-java-autocomplete-not-working-259


