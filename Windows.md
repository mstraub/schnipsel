# attrib

Change file attributes from the CLI

Remove `hidden` and `system file` attribute must happen at the same time)
```sh
attrib -s -h "E:\ahoi backup"
```

# robocopy

Reliably copy large directories, similar to `rsync`

```sh
robocopy D:\ "E:\ahoi backup" /E /XA:S /XD "`$RECYCLE.BIN" "System Volume Information" /Z /R:5 /W:5 /TEE /LOG:robocopy.log
```

- `/E` copies subdirectories, including empty ones
- `/Z` restartable mode
- `/R:5 /W:5` Retry up to 5 times and wait 5 seconds inbetween
- backtick before $ only required in powershell
- `/XA:S` excludes system files, but apparently not folders? so we exclude the two obvious candidates manually with `/XD`.

# winget

Since ~2020 Windows has an official package manager: `winget` 

```shell
winget search <PKG>
winget install <PKG_ID>
```

# open user home

type `%UserName%` in the address bar

# Word

- non-breaking space: `Ctrl+Shift+Space`
- line-break hints: `Ctrl+-`