Two important tools can be used for searching files in Linux: `find` and `locate`.

The big difference between those two:
- `find` searches the system *live*
- `locate` uses a database that is typically updated only once a day

# find

In the simplest invocation `find` just prints all files in the current directory and all its subdirectories (similar to `tree`).
```bash
find                        # print all filenames in dir + subdirs
find dir1 dir2              # print many dirs + subdirs
```

To filter the files an "expression" must be added as last parameter.
An expression can be quite complex and can consist of options, tests and actions.
The most straight-forward test is `-name`:
```bash
find dir1 dir2 -name "*.jpg"      # find all .jpg files in both directories
find dir1 dir2 -iname "*.jpg"     # same, but ignore case (e.g. also find .JPG)
```

Some examples for **options**
```bash
find dir -maxdepth 1              # find all files in dir (but not its subdirs)
find dir -xdev -name "*.jpg"      # find files only in the current file system
                                  # (i.e. does not search NFS shares or /proc)
```

Some examples for **tests**:

Note, that numeric arguments are specified as:
- `+n` for greater than n
- `-n` for less than n
- `n` for exactly n

```bash
find dir -size +10M               # find files bigger than 10 megabytes
find dir -size -50c               # find files smaller than 50 bytes (c=byte, k=kilobyte, M=Megabyt, G=Gigabyte)
find /bin -perm -u=s              # find all executables in /bin that have the suid bit set for its owner
find dir -perm 644                # find files with permissions being exactly 0644
find /usr/bin -executable -type f # find all executable files in /usr/bin
```

**Time based tests** are possible for days or minutes:

- `-ctime` / `-cmin`: last file status change (not exactly creation time but similar)
- `-mtime` / `-mmin`: last file modification
- `-atime` / `-amin`: last file access

```bash
find dir -atime -3                # find files accessed in the last two days
find dir -mmin +10                # find files modified more than 10 minutes ago
```

Some examples for **actions**: delete files, execute commands
```bash
find dir -exec command {} \;      # execute a command for each of the found file - {} will be replaced with the filename
find dir -exec command {} +       # execute one command for all of the found files - {} will be replaced with the filenames
find dir -execdir command {} \;   # same as -exec, but the execution directory is the directory of the file, not the starting directory
find dir -delete                  # delete found files
```

http://linux.101hacks.com/linux-commands/find-command-examples/

# locate

In contrast to `find`, which traverses the file system on each invocation,
`locate` uses a precalculated database. The database is typically updated
daily via a cronjob. This makes `locate` very fast but also slightly out of date
for files created or moved during a certain day.

```bash
locate pattern         # search for a pattern (put it in quotes to avoid shell expansion)
                       # by default the pattern can be contained anywhere in the file name,
                       # similar to the regex .*pattern.*
locate -b pattern      # search for the pattern in the (b)asename only, i.e. not in the parent path
locate -i -e pattern   # (i)gnore case and only print (e)xisting files,
                       # i.e. not moved or deleted since the last db update
locate -r /exactname$  # use this regex to search for an exact filename
sudo updatedb          # manually start an update of the database
```
