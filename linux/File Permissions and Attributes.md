Each file in Linux has

- a type
- an owner
- a group (similar to owner)
- permissions (can owner/group/others read/write/execute?)
- attributes

Note, that for `root` can not only change file permissions but they are also do not apply (except for the execution of files)!
E.g. `root` can can always write/delete write-protected files or create files in in a write-protected directory.
To protect files / directories from unwanted changes by `root` attributes can be used.

The output of `ls -l` e.g. looks like `drwxrwxrwx`.
The first character identifies the file type, then there follow three access-tuples (for owner, group, others).

List of possible values for the file type (first character):

- `-` file
- `d` directory
- `l` symlink
- `c` character device (e.g. `/dev/random`)
- `b` block device (e.g. `/dev/sda1`)
- `p` fifo
- `s` socket

# Basic Tools

## chown - change owner

With `chown` both the owner and the group can be changed. Simple usage examples:

```bash
chown myuser file.txt           # change the owner of file.txt to 'myuser'
chown myuser:mygroup file.txt   # change the owner of file.txt to 'myuser' and the group to 'mygroup'
chown :mygroup file.txt         # only change the owner of file.txt to 'mygroup'
chown -R myuser directory       # change the owner of directory recursively to 'myuser'
```

If a symbolic link is given, by default the referenced file is changed, not the link itself.
Use `-h` to avoid dereferencing and to change the owner of the symlink itself.

In recursive mode symbolic links not traversed by default (`-P`).
With `-L` every encountered symlink can be traversed. (FIXME ausprobieren)

To only change the owner for files owned by a certain user and/or group use the `from` option with the `user:group` syntax:

```bash
chown -R --from=myuser:mygroup otheruser:othergroup directory
```

[More examples](http://www.thegeekstuff.com/2012/06/chown-examples)

## chgrp - change group

FIXME

## chmod - change permission

A combination of the letters `**ugoa**` controls which users' access to the file will be changed:

- `u` the user who owns it
- `g` other users in the file's group
- `o` other users not in the file's group
- `a` all users

The letters `**rwxXst**` select file mode bits for the affected users:

- `r` read
- `w` write
- `x` execute (or search for directories)
- `X` execute/search only if the file is a directory or already has execute permission for some user
- `x` set user or group ID on execution
- `t` restricted deletion flag or sticky bit

The letter for affected users and the ones for the mode can be combined with one of `=+-`. (equals, add, remove)

```bash
chmod o+rw file    # add permissions for other (everybody) to read and write file
```

Explicit setting of all permissions is also possible in four octal numbers (e.g. 0644 or 0755 are common).
The first represents the suid/sgid/sticky bits, the other three permissions for ugo.

- number 1: read=4, write=2, execute=1
- numbers 2-4: suid=4, sgid=2, sticky=1

```bash
chmod 0644 file    # do not set suid/sgid/sticky bit but allow owner rwx and group/other rw
```

Some advanced examples:

```bash
chmod u+srw,g+r file  or  chmod 4740 file # set suid, read and write access for user,
                                          # only read acces for group, nothing for other
chmod -R a+awX `<dir>` # recursively give read/write access to a directory tree
                       # (executable bit is only set for directories - and files where
                       # one of ugo already has the executable bit set)
```

## chattr - change attributes

```bash
chattr - change file attributes (a=append only, i=immutable, s=secure deletion,..)
lsattr file       # view file attributes
chattr +a file    # allow only appends to file (requires root to (un)set)
```

## setfacl - set file access control lists

ACLs are advanced methods for controling file access.

Recursively set permissions for all existing and *to be created* files:
```bash
setfacl -PRdm u::rwx,g::rw,o::r /opt/thedirectory
```

>[!WARNING] the ACLs are not always applied, e.g. they are ignored for files created by `scp`

# SUID, SGID, Sticky Bit

## suid, sgid on files

When users execute executables with those bits, the effective suid/sgid of the process is the owner of file.
This is why regular users can edit `/etc/shadow` with `/bin/passwd`.

## sgid on directory

All files will get the same group as directory, and all subdirectories will inherit the sgid bit.

## sticky bit

In sticky directories only owners of a file/directory and root can delete it (as in /tmp)

see also: http://www.bashguru.com/2010/03/unixlinux-advanced-file-permissions.html

## umask

The [umask](http://en.wikipedia.org/wiki/Umask) determines which file permissions are set for files and directories when they are created.
Note, that the umask definition is negative, i.e. will be subtracted from the default permission (e.g. 666 for files).
A typical umask is 022: 666 - 022 = 644, which means the owner can read and write to a file, group and other can only read.

`umask` is a bash built-in to set & view the current umask of a user.
Typically it is invoked in /etc/profile (or a file referenced from there).

```bash
umask              # print currently active umask (octal)
umask -S           # print currently active umask (symbolic output)
umask 0xxx         # set a umask (octal)
```

# bindfs

Mount a directory to another location and alter permission bits.
This works more reliably then ACLs, i.e. also for `scp`.

Force all files created in a folder to a certain owner, group and permissions (yep, the directory is bound over itself):
```sh
sudo bindfs --create-for-user=root --create-for-group=mytrips --create-with-perms=g+rwX /opt/x /opt/x
```

And the same as entry in `etc/fstab`:
```
/opt/x /opt/x fuse.bindfs create-for-user=root,create-for-group=mytrips,create-with-perms=g+rwX 0 0
```

