# System Configuration

## Bootloader

System startup works as follows:
 1.  The BIOS decides on a boot device and loads the boot loader in its Master Boot Record (MBR), typically 512 bytes small.
 2.  The boot loader (e.g. GRUB2) then starts the Linux kernel (and provides kernel parameters and boot options).
 3.  Optionally: an init RAM disk (initrd) contains the most important kernel modules to load e.g. RAID controllers or file systems.
 4.  After the kernel is initialized it starts the mother of all processes: `init`

### LILO

LILO stores the disk sector where the kernel can be found in the boot loader. Therefore

- the `/boot` partition must be located before sector 1024 of the hard disk, otherwise LILO can't address it;
- LILO must be reinstalled after each kernel update;
- it does not matter which filesystem is used, LILO completely ignores it

The main configuration file is `[/etc/lilo.conf](http://linux.die.net/man/5/lilo.conf)`

### GRUB Legacy

GRUB uses two stages:
 1.  stage1: the 512B in the MBR contain tools to initiate stage2
 2.  stage2: with tools in `/boot` (supported filesystems are e.g. ext2/3/4, reiserfs) the Linux kernel is started

```bash
cat /boot/grub/menu.lst         # configuration file (with meta commands that control the behaviour of update-grub)
cat /boot/grub/device.map       # device mapping file
cat /etc/grub.conf              # options, parameters,..
update-grub                     # tool that parses and write menu.lst
```

http://wiki.ubuntuusers.de/menu.lst

### GRUB2

Configuration:

```bash
cat /boot/grub/grub.cfg         # generated configuration file (overwritten with update-grub, kernel-updates,..)
cat /etc/default/grub           # main configuration file
ls /etc/grub.d                  # directory with additional configuration files
cat /etc/grub.d/40_custom       # custom entries that update-grub will put in grub.cfg
```

With the following commands GRUB2 is installed in two steps - (1) generate a grub.cfg, (2) install GRUB to the MBR. Step 1 must be invoked after each kernel update - otherwise the kernel will not show up as an option during boot.

```bash
update-grub                     # generate grub.cfg (searches for installed OSes)
grub-mkconfig -o /boot/grub.cfg # same as update-grub
grub-install                    # install boot loader to MBR or partition
```

http://www.gnu.org/software/grub/manual/grub.html
https://help.ubuntu.com/community/Grub2



## Shared Libraries

Shared libraries are used by most programs to save disk space, RAM space and for faster startup times (when library was already loaded by another program). Only programs for minimal environments are statically linked to avoid dependencies.

```bash
ldd program             # print required dynamic libraries of an executable
cat /etc/sd.so.conf     # search paths for ldconfig (/lib and /var/lib are implicit)
cat /etc/sd.so.cache    # cache with available dynamic libraries
ldconfig                # update sd.so.cache with libraries found in paths from sd.so.conf and $LD_LIBRARY_PATH
```

## Hostname

The host name is persisted in `/etc/hostname` and can be shown with `hostname` or (using the systemd-way) with `hostnamectl`.

To change the host name
 1.  use `sudo hostname NAME` to set the host name now (would not survive a reboot)
 2.  edit `/etc/hostname` to set it permanently
 3.  edit `/etc/hosts` to properly map the new host name to localhost

Instead of the first two steps you can also use

```bash
hostnamectl set-hostname NAME
```


## Date & Timezone

### System Time

Linux has two clocks: hardware clock (aka BIOS clock) and a software clock (maintained by the Linux kernel). The software clock is initialized with the time of the hardware clock during boot time and written back to the hardware clock on shutdown. [More details](http://www.tldp.org/LDP/sag/html/keeping-time.html)

##### hwclock

The hardware clock can be read and set (by root only) with the `hwclock` command. The parameters `%%--utc%%` and `%%--localtime%%` tell if the hardware clock is / should be set to UTC or local time.

**Note, however, that hwclock always prints and expects (when setting) date in local time.**

```bash
hwclock (--show)         # show current setting
hwclock --systohc --utc  # set hardware clock from system clock (and use UTC)
hwclock --hctosys        # set system clock from hardware clock
```

Completely manual usage:

```bash
hwclock --set --date="2012-04-19 16:45:05" --utc
hwclock --set --date="2012-04-19 16:45:05" --localtime
```

The current time zone of the hardware clock is specified in `/etc/adjtime`, or UTC if the file is missing.

##### date

Changing the system clock (time + date)

```bash
date -d "2012-12-12T12:00" # print a date
date -s "2012-12-12T12:00" # set a date + time as system time
date +%T -s "14:30"        # only set the system time (not the date)
```

The string format interpreted as date is extremely versatile and described in `info date`, here are some examples:

```bash
date -d @1234567890       # Unix time / seconds since epoch
date -d "2014-01-01"      # ISO 8601
date -d "next Thursday"
date -d "2 years"         # now + 2 years
date -d "2 years ago"
```

The output format can be adjusted with format strings:

```bash
date +'%s'                # Unix time / seconds since epoch
date +'%Y-%m-%dT%H:%M:%S' # in ISO 8601
```

#### Network Time Protocol (NTP)

The best idea is to set the hardware clock via BIOS and let an NTP daemon take care of adjusting the system time. [Ubuntu preinstalls](https://help.ubuntu.com/14.04/serverguide/NTP.html) `ntpdate`, but the more advanced `ntpd` is also available.

NTP synchronizes your system time with the ones from time servers (if it's not off by more than a few minutes). The NTP pool at `pool.ntp.org` provides a fail-safe address because it actually uses many different servers behind this address.

##### ntpdate(-debian)

`ntpdate` is the standard tool to synchronize the software clock with NTP once. It requires a server to be provided at each call.

`ntpdate-debian` is a slightly modified version that can be configured via `/etc/default/ntpdate`. Ubuntu executes this program every time a network becomes available, see `/etc/network/if-up.d/ntpdate`.

```bash
sudo ntpdate-debian                # get NTP time from the configured server
sudo ntpdate pool.ntp.org          # get NTP time from a reliable cluster of NTP servers
```

##### ntpd

The ntp daemon always runs in the background and can correct drifts slowly to avoid jumps in time.

It can be configured (e.g. change the NTP servers) via `/etc/ntp.conf`.

`ntpq` can query the status of the ntp daemon:

```bash
ntpq -p                            # print ntp peers & their state
```
### System Time Zone

The system time zone is set in two places (which should match):

- `/etc/localtime` contains the binary time zone specification (one of the files in `/usr/share/zoneinfo`)
- `/etc/timezone` contains the human-readable POSIX timezone name

##### tzselect

`tzselect` is an interactive program to let a user retrieve the appropriate POSIX timezone string for his location, e.g. "Europe/Vienna" for Austria. It does not change the time zone!

#### Setting via tzconfig

`tzconfig` was used to set the timezone, but on modern Debian / Ubuntu systems we use `dpkg-reconfigure`, which will run through the same questions as `tzselect`:

```bash
dpkg-reconfigure tzdata
```

This program copies the appropriate (binary) time zone info file from `/usr/share/zoneinfo` to `/etc/localtime`.

#### Other methods

Set the environment variable `TZ` to the appropriate timezone name, e.g. in ~/.profile

```bash
TZ='Europe/Vienna'; export TZ
```

[Still more methods...](http://www.wikihow.com/Change-the-Timezone-in-Linux)

### Quick Fix

Quickly fix the system time & timezone:

```bash
dpkg-reconfigure tzdata      # set proper timezone
ntpdate-debian               # get current time via NTP
hwclock --systohc --utc      # set hardware clock to (UTC) time retrieved from via NTP
```

## Locales

[Locales](http://www.gnu.org/software/gettext/manual/html_node/Locale-Environment-Variables.html#Locale-Environment-Variables) enable internationalization and define e.g. number formats (LC_NUMERIC) or date and time formats (LC_TIME).

The default is set with LANG, which can be overriden with the more specialized LC_*. LC_ALL overrides everything else, and should not be set except for the execution of commands.

```bash
locale                  # print current locale settings
locale -a               # print names of available locales
locale -k LC_TIME       # print current formats for a locale setting
locale charmap          # print currently used charmap (e.g. UTF-8)
```

### Generate locales

All available locales are listed in `/usr/share/i18n/SUPPORTED`. To use a locale it must be generated first with `locale-gen`:

```bash
locale-gen de_AT.UTF-8
```

Note, that locale-gen fails silently if an invalid locale is given. (return code 1)


### Permanent settings

Locale variables are set in the configuration file `/etc/default/locale`. A great configuration for using the English language with ISO date/time format and european paper size (A4) is:


	LANG="en_US.UTF-8"
	LC_TIME="en_DK.UTF-8"
	LC_MEASUREMENT="en_DK.UTF-8"
	LC_PAPER="de_AT.UTF-8"


**Note**, that some programs ignore the settings in LC_PAPER and use `/etc/papersize` instead. Set a consistent format there as well, e.g. `a4`.

**Note for KDE:** the locale configuration tool will create the file `~/.kde/env/setlocale.sh`, which overrides `/etc/default/locale` - delete undesired settings there.

### Temporary settings

To set a certain locale for one command only:

```bash
LC_TIME="en_DK.UTF-8" locale -k LC_TIME        # use specific locale for one command
```

It may also be useful to override all locale settings and use the most basic locale available on every machine: `C` or `C.UTF-8`:

```bash
LC_ALL=C.UTF-8 locale -k LC_TIME
LC_ALL=C.UTF-8 ssh host
```

[More in-depth explanations](http://unix.stackexchange.com/questions/87745/what-does-lc-all-c-do/87763#87763)

