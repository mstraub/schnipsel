https://zfsonlinux.org/

First install ZFS - it is not included by default because its licensing situation does not allow full kernel integration.

```bash
sudo apt install zfsutils-linux
```

Check names and status of available pools:
```bash
zpool status
```

Show all ZFS properties of a pool:
```
zfs get all my_pool_name
```

# ZFS on single external hard drive

> [!WARNING] unfortunately Ubuntu 22.04 does not autodetect / mount a plugged in ZFS drive.

The goal was to have an external hard drive with the capability to **detect and repair bit rot**.
So we use ZFS which can detect bit rot through [checksums](https://openzfs.github.io/openzfs-docs/Basic%20Concepts/Checksums.html) and repair them with [redundand copies of our data](https://openzfs.github.io/openzfs-docs/man/master/7/zfsprops.7.html#copies).


```bash
sudo fdisk -l # list devices to decide where to put the pool
# create pool on a single disk - it is auto-mounted at /my_pool_name
sudo zpool create my_pool_name /dev/sdx
sudo zfs set copies=2 my_pool_name # store redundand copies
```

Sources: http://jenpeterson.net/zfs-blog/ - ZFS on external drive

### TODO
- mount with user access rights
- umount
- auto-mount via gnome?
- scrub
