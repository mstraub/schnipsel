# System Startup

## Runlevels

Runlevels were used in older distros to only partially start up the system. From single-user mode without networking, to multi-user mode with networking. Nowadays most distros don't use many different runlevels much anymore.

Runlevels:

- 0: halt
- S: single user mode (often: 1)
- 2-5: differs between distros, Ubuntu uses 2 as default
- 6: reboot

##### runlevel

The previous and current runlevel can be checked with `runlevel`. N stands for no previous runlevel.

##### telinit

Switching runlevels is done with `telinit`. In most distros `init` does the same.

```bash
telinit 2        # switch to runlevel 2
telinit 6        # reboot system
```

##### shutdown

With `shutdown` reboots or shutdowns can be timed with either 'now', +`<minutes>` (e.g. +10), or hh:mm (e.g. 19:20).
Other logged in users will be notified of a (pending) shutdown, no logins are possible five minutes before shutdown, and it is possible to specify a customized (optional) message.

```bash
shutdown -h now                                  # shutdown (halt or power off) now
shutdown -P now                                  # shutdown and power off now
shutdown -r +10 "message"                        # reboot in 10 minutes and send message
shutdown -k 20:00 "possible power outage at 8"   # 'fake' a shutdown. messages are sent, but no shutdown will be done
shutdown -c                                      # cancel pending shutdown
```

##### reboot, halt, poweroff

An alternative to the shutdown command are the following commands:

```bash
reboot
halt           # shut down OS, but do not power off (via ACPI)
poweroff       # shut down OS and power off (what you usually want)
```


## Init Daemons

The binary `/sbin/init` is started first (hardcoded into the kernel) and therefore the mother of all processes.
It starts all required processes during boot.

The 'original' Sysvinit is replaced in most distros as of 2013 with init systems that are event based (asynchronous).

##### sysvinit

Configuration via

```bash
/etc/init.d       # directory with startup scripts (nowadays still: legacy scripts)
/etc/inittab      # set default runlevel
```

##### upstart

Upstart is a Ubuntu-only event-based replacement for the original init daemon and brings some new tools and commands, but was replaced with systemd since Ubuntu 15.05.

Init scripts and scripts for services are contained in `/etc/init`. Services can be controlled like this:

```bash
service myservice start       # equivalent: start myservice
service myservice stop        # equivalent: stop myservice
service myservice reload      # equivalent: reload myservice
service myservice restart     # equivalent: restart myservice
service --status-all          # get status of all services
```

##### systemd

Modern event-based replacement for `init`, used by most distributions in 2015.

Init scripts are represented as [service units](https://www.freedesktop.org/software/systemd/man/systemd.service.html) in the form of `.service` files, which can be found in `/lib/systemd/system` (preinstalled) and `/etc/systemd/system` (to be edited by the admin).

Example of a `.service` file with the the three sections:


```
[Unit]
Description=Foo

[Service]
ExecStart=/usr/sbin/foo-daemon

[Install]
WantedBy=multi-user.target
```

Starting and stopping services works as follows:

```bash
systemctl start myservice
systemctl stop myservice
systemctl status myservice
```

Logs of a service can be accessed via `journalctl`:

```bash
journalctl -u myservice
```

> [!TIP] any child processes a service created are [killed](https://unix.stackexchange.com/questions/204922/systemd-and-process-spawning-child-processes-are-killed-when-main-process-exits) once it is stopped 

**User Units**

The system-wide systemd instance requires sudo permissions to start/stop services.
There is also a **per-user-instance**:

- units are put into `~/.config/systemd/user/`
- start them with `systemctl --user start myservice`

> [!NOTE] user units are completely independent from system services
> i.e. you can not depend on those services (such as `sleep.target`)
