Email basically works as follows.

The sender uses a **Mail User Agent (MUA)**, or email client, to send the message through one or more **Mail Transfer Agents (MTA)**, the last of which will hand it off to a **Mail Delivery Agent (MDA)** for delivery to the recipient's mailbox, from which it will be retrieved by the recipient's email client, usually via a POP3 or IMAP server.

Commonly used programs are:

- MUA: Mozilla Thunderbird, Microsoft Outlook,..
- MTA: sendmail (the original), postfix, exim, postmaster, qmail, smail
- MDA: procmail, maildrop

# Set up a Local Email Server

```bash
apt-get install postfix
dpkg-reconfigure postfix  (basically edits /etc/postfix/main.cf)
```

No matter which MTA is used, they all share:

- `sendmail` as command to send emails
- `/etc/aliases`

## aliases

With `/etc/aliases` emails to local recipients can be redirected to one or multiple recipients.

The format for each line is:


	#name: value1, value2, ...
	postmaster: alice
	root: alice, bob


After changes to `/etc/aliases` the command `newaliases` or `sendmail -bi` must be called.
It updates the alias-database, which is used by sendmail et al instead of the raw text file.

Each user can create redirections for only his/her account in `~/.forward` using the same format.

More in the [Ubuntu server guide](https://help.ubuntu.com/14.04/serverguide/postfix.html)


# Send Emails

An email consists of a header and a body, separated by a new line. A very simple Email:


	Subject: Hello World

	This is the body of my first email.


To send it the command `sendmail` can be used (no matter which MTA is actually used):

```bash
sendmail recipient@example.com < email.txt
sendmail -bm -f sender@example.com recipient@example.com < email.txt
```
# Read Emails

Incoming emails are stored in the mail spool in `/var/mail/username`. The spool can e.g. be inspected with the command line tool `mail`.

Typically MUAs move emails from the spool to a different location. MUAs that support mail spools are e.g. mutt and pine (command line) or Mozilla Thunderbird (create a 'Unix Mailspool (Movemail)' account).

