Official docs: https://btrfs.readthedocs.io

Install BTRFS
```bash
sudo apt install btrfs-progs
```

Create filesystem on the whole disk (without partitions) using data duplication (similar to `copies=2` in ZFS)
```bash
sudo mkfs.btrfs -d dup -L <LABEL_NAME> /dev/sdx -f
```
Then `mount` and `umount` it as required.

>[!NOTE] auto-mounting of external drives with BRTFS works as expected (contrary to ZFS)

Show summary of all BTRFS filesystems (also ones that are not mounted)
```bash
sudo btrfs filesystem show
sudo btrfs filesystem show <MOUNTPOINT>
```

Show summary and detailed info about used / free space
```bash
sudo btrfs filesystem du -s <MOUNTPOINT>
sudo btrfs filesystem usage <MOUNTPOINT>
```
Show name / rename filesystem
```bash
sudo btrfs filesystem label <MOUNTPOINT>
sudo btrfs filesystem label <MOUNTPOINT> <NEW_LABEL>
```
Show and change properties
```bash
btrfs property list <PATH>
btrfs property get <PATH> ro # get read-only property
btrfs property set <PATH> ro true # set read-only=true
```

## Subvolumes and Snapshots

[Subvolumes and snapshots](https://btrfs.readthedocs.io/en/latest/Subvolumes.html) are independently mountable POSIX filetrees.
They show up in a file browser as directories and can be renamed, moved, or deleted via the file browser.

List subvolumes (and snapshots) under path
```bash
sudo btrfs subvolume list <PATH>
sudo btrfs subvolume list -s <PATH> # only shapshots
sudo btrfs subvolume list -uq <PATH> # show uid of self and parent (if snapshot)
```

Create a new subvolume (the path must not exist yet).
```bash
btrfs subvolume create <PATH>
```

Delete a subvolume
```bash
btrfs subvolume delete <PATH>
```

Create a read-only snapshot (of a subvolume):
```bash
btrfs subvolume snapshot -r <SUBVOLUME_PATH> <SNAPSHOT_PATH>
```

### My external drive setup

The goal was to have an external hard drive with the capability to **detect and repair bit rot**.
So we use BTRFS which can detect bit rot through checksums and repair them with redundant copies of our data.

Let's set up BTRFS with data duplication and a subvolume `main` containing
the main (current) version of all data.

```bash
lsblk # find <PATH> to the drive, e.g. /dev/sdb (or use df, cfdisk,..)
sudo mkfs.btrfs -d dup -L <LABEL> <PATH>
mkdir /tmp/x
sudo mount <PATH> /tmp/x
cd /tmp/x
sudo btrfs subvolume create main
sudo chown <USER>:<GROUP> main
```

Create a read-only snapshot of the subvolume *main*
```bash
sudo btrfs subvolume snapshot -r main snapshot_`date -u +'%Y-%m-%d'`
```

The resulting file layout:
```
.
├── main
├── snapshot_2023-12-27
└── snapshot_...
```

Regularly `scrub` the filesystem, which finds (and fixes since we use `dup` mode) errors:
```bash
sudo btrfs scrub start /dev/sdb
sudo btrfs scrub status /dev/sdb
```

Quickly see which files changed between snapshots:
```bash
sudo btrfs send --no-data -p snapshot_2024-01-20 snapshot_2024-06-16/ | sudo btrfs receive --dump
```
https://serverfault.com/questions/399894/does-btrfs-have-an-efficient-way-to-compare-snapshots

Send initial snapshot to the backup disk:
```bash
sudo btrfs send /media/mstraub/evodada/snapshot_2024-01-20/ | pv | btrfs receive /media/mstraub/evobup/
```
.. and subsequent ones (always relative to a `-p` parent) 

> [!TODO] TODO didn't work yet
> ERROR: snapshot: cannot find parent subvolume)
> also: now that I have the backup twice on the second drive data is not shared between the two backups (i.e. `btrfs fi du -s` shows 0 MB shared data)
```bash
sudo btrfs send -p /media/mstraub/evodada/snapshot_2024-01-20/ /media/mstraub/evodada/snapshot_2024-12-31/  | pv | btrfs receive /media/mstraub/evobup/
```
see https://alex.barton.de/2021/07/btrfs-dateisysteme-mit-snapshots-sichern/

The backup script with `rsync` looks similar to this:
```bash
#!/bin/bash

BASE_ARGS="-a --delete --itemize-changes"
ARGS="$BASE_ARGS --dry-run"
if [ $# -gt 0 ]; then
  if [ $1 = "yes" ]; then 
    ARGS=$BASE_ARGS
  fi
fi

echo "running rsync with: $ARGS"

rsync $ARGS /home/evod/xxx/ /media/evod/evodada/main/live/yyy/
```
### Issues

- Nautilus (Ubuntu 22.04) can not send files to trash from within a subvolume. Which is annoying, because all my stuff on the drive is within a subvolume :/ (https://www.reddit.com/r/btrfs/comments/ul2t9g/no_trash_on_btrfs_subvolumes_other_than_root_and/)
