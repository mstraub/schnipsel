# Unix-Style

## tar - (uncompressed) tape archives

`tar` is able to store multiple files in a single (by default uncompressed) archive.

```bash
tar -cf archive.tar a b c            # create tar archive of file/folders a, b, c
tar -tf archive.tar                  # list contents of archive (actually: tests integrity of archive)
tar -xf archive.tar  -C destination  # extract content of tar archive into folder destination (otherwise: current directory)
```

## Compression Algorithms

In the Linux ecosystem typically `tar` files are compressed either with
**gzip** (.gz), **bzip2** (.bz2) or **xz** (.xz, which uses LZMA compression).

Gzip is the most commonly used format, but according to [this in-depth comparison](https://www.rootusers.com/gzip-vs-bzip2-vs-xz-performance-comparison) xz is the superior format.
Xz is fast to decompress, achieves high compression rates and also has reasonable compression times until level 2 or 3.

The following commands (un)compress a single file and then remove the old (un)compressed file.

```bash
gzip my.txt           # creates the gzipped file my.txt.gz and remove my.txt
gunzip my.txt.gz      # recreates my.txt and removes my.txt.gz; same as gzip -d
gunzip -k my.txt.gz   # recreates my.txt but keeps my.txt.gz; same as --keep
```

The commands `bzip2` + `bunzip2` and `xz` + `unxz` work in the same fashion.

For all compression formats there is a `cat`-style alias which allows for the creation of an uncompressed data stream:

```bash
zcat                 # stream uncompressed gzip file to stdout, same as gzip -cd
bzcat
xzcat                # same as xz --decompress --stdout
```

## Compressed Archives

Often you want to compress archives as e.g. `tar.gz` files.
This can in theory be done with separate calls to `tar` and `gzip`,
in practice we directly use `tar`:

```bash
tar -czf archive.tar.gz a b c                  # create gzipped archive
tar -cf archive.tar a b c && gzip archive.tar  # does the same
tar -xzf archive.tar.gz                        # extract gzipped archive
gunzip archive.tar.gz && tar -xf archive.tar   # does the same
```
For taring or untaring compressed archives use the following flags:
- gzip: `-z`
- bzip2: `-j`
- xz: `-J`

> FIXME:  `cpio` can copy files to and from archives FIXME


# Windows-Style

In the Windows world **ZIP** (`.zip`) is prevalent, sometimes **RAR** (`.rar`) or **7z** archives (`.7z`) can be found as well.

## ZIP

The `unzip` tool only supports very old versions of the [zip file format](https://en.wikipedia.org/wiki/ZIP_(file_format))  (up to v4.6 from 2001 to be exact)
For newer archives you are likely to encounter errors like this:

      skipping: the-file.txt  need PK compat. v5.1 (can do v4.6)

`7z` supports more recent zip format versions (rar and 7zip as well) though:

```bash
7z l file.zip # list contents
7z x file.zip # extract files (to current dir)
7z x file.zip -ooutputDir # extract files to specific dir
```

Installing the `p7zip-full` package also adds support for newer zip archives to the GUI tools in Ubuntu.

