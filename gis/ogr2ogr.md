`ogr2ogr` is a command-line conversion tool between various GIS formats and part of the open source geospatial library [GDAL](https://gdal.org). Actually it can do more than just conversions: clipping, filtering,..

Print a list of supported conversion formats with `ogr2ogr --long-usage`

# Usage
## Merge

### Geopackes

Add layers of another geopackage to this geopackage:
```bash
ogr2ogr -f GeoPackage -append this.gpkg another.gpkg
```

### Shapefiles

```bash
ogr2ogr merged.shp file1.shp                                    # create a copy of file1
ogr2ogr -update -append merged.shp file2.shp -nln merge         # append file2
```

### `gpx` into Shapefile

```bash
#!/bin/bash
SAVEIFS=$IFS
IFS=$(echo -en '\n')
for f in *.gpx
do
  ogr2ogr gpx.shp -append "$f" tracks -fieldTypeToString DateTime
done
IFS=$SAVEIFS
```

## Export specific layer from Geopackage

### To `GeoJSON`

```bash
ogr2ogr -f GeoJSON export.geojson source_geopackage.gpkg layer_name -lco WRITE_BBOX=yes -lco RFC7946=yes
```
Write a bbox and conform to the *new* GeoJSON spec.
`-lco` layer creation options are format specific, see [GDAL's GeoJSON driver options](https://gdal.org/drivers/vector/geojson.html)

### To `CSV`

```bash
ogr2ogr -f CSV export.csv source_geopackage.gpkg layer_name -lco GEOMETRY=AS_XY -t_srs "EPSG:31256"
```

`-t_srs` specifies the output projection
`-lco` layer creation options are format specific, see [GDAL's CSV driver options](https://gdal.org/drivers/vector/csv.html)

## CSV (WKT) to Geopackage

- with a column named `wkt`:
- using `-a_srs` to assign (not reproject) a CRS
```bash
ogr2ogr -f GPKG output.gpkg input.csv -oo GEOM_POSSIBLE_NAMES=wkt -a_srs 'EPSG:3416'
```

## Convert OpenStreetMap

Convert an OSM `.xml` or `.pbf` to a different geospatial format:

```bash
ogr2ogr -f GPKG output.gpkg input.osm.pbf 
```

If you need more (or other) attributes as fields in the resulting files adjust the system-wide config file `/usr/share/gdal/osmconf.ini`.

> **Note regarding QGIS**: this file is also used when dragging and dropping OSM files into QGIS, because it just calls GDAL in the background!

Or tell `ogr2ogr` to use a specific copy of that file:

```bash
OSM_CONFIG_FILE=osmconf_custom.ini ogr2ogr -f GPKG output.gpkg input.osm.pbf 
```

[See GDAL's OSM driver options](https://gdal.org/drivers/vector/osm.html)

